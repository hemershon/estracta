class Teacher < ApplicationRecord
    mount_uploader :photo, PhotoUploader
    has_one :user

end

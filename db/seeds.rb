# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create email: 'ieda@teste.com', kind:'professor',password: 123456
User.create email: 'katia@teste.com', kind:'professor', password: 123456
User.create email: 'simao@teste.com', kind:'professor', password: 123456
User.create email: 'maria@teste.com', kind:'professor',password: 123456
User.create email: 'leila@teste.com', kind:'professor', password: 123456
User.create email: 'telma@teste.com', kind:'professor', password: 123456
# Aluno 
User.create email: 'carlos@teste.com', kind:'aluno',password: 123456
User.create email: 'manoel@teste.com', kind:'aluno',password: 123456
User.create email: 'azevedo@teste.com', kind:'aluno',password: 123456
User.create email: 'beto@teste.com', kind:'aluno',password: 123456
User.create email: 'luiz@teste.com', kind:'aluno',password: 123456
User.create email: 'kalil@teste.com', kind:'aluno',password: 123456
User.create email: 'felipe@teste.com', kind:'aluno',password: 123456


Student.create name: 'Carlos silva', birth: '23/11/2000', mother:'Felicia Simão Silva', dad:'Hemershon Silva', classe:'6 ano', round:'manhã', status: 'ativo', kind:'aluno'
Student.create name: 'Manoel de jesu', birth: '23/11/2000', mother:'Felicia Simão Silva', dad:'Hemershon Silva', classe:'6 ano', round:'manhã', status: 'ativo', kind:'aluno'
Student.create name: 'Azevedo', birth: '23/11/2000', mother:'Felicia Simão Silva', dad:'Hemershon Silva', classe:'6 ano', round:'manhã', status: 'ativo', kind:'aluno'
Student.create name: 'Beto', birth: '23/11/2000', mother:'Felicia Simão Silva', dad:'Hemershon Silva', classe:'6 ano', round:'manhã', status: 'ativo', kind:'aluno'
Student.create name: 'Luiz silva', birth: '23/11/2000', mother:'Felicia Simão Silva', dad:'Hemershon Silva', classe:'6 ano', round:'manhã', status: 'ativo', kind:'aluno'
Student.create name: 'Kalil Rabelo', birth: '23/11/2000', mother:'Felicia Simão Silva', dad:'Hemershon Silva', classe:'6 ano', round:'manhã', status: 'ativo', kind:'aluno'
Student.create name: 'Felipe Rabelo', birth: '23/11/2000', mother:'Felicia Simão Silva', dad:'Hemershon Silva', classe:'6 ano', round:'manhã', status: 'ativo', kind:'aluno'


# Professor
Teacher.create name: 'katia sekeff', kind:'professor', subject:'Português', status:'ativo'
Teacher.create name: 'José simao', kind:'professor', subject:'Matemática', status:'ativo'
Teacher.create name: 'Maria sekeff', kind:'professor', subject:'Geográfia', status:'ativo'
Teacher.create name: 'Leila Silva', kind:'professor', subject:'História', status:'ativo'
Teacher.create name: 'Telma budaruich', kind:'professor', subject:'Ed.Física', status:'ativo'
Teacher.create name: 'Ieda Simao', kind:'professor', subject:'Ens. Religioso', status:'ativo'

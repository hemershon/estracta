class CreateTeachers < ActiveRecord::Migration[6.0]
  def change
    create_table :teachers do |t|
      t.string :photo
      t.string :name
      t.string :subject
      t.integer :status

      t.timestamps
    end
  end
end

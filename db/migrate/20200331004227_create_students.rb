class CreateStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :students do |t|
      t.string :photo
      t.string :name
      t.string :birth
      t.string :mother
      t.string :dad
      t.string :classe
      t.string :round
      t.string :status

      t.timestamps
    end
  end
end

class AddKindToStudent < ActiveRecord::Migration[6.0]
  def change
    add_column :students, :kind, :integer
  end
end

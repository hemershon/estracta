class AddKindToTeacher < ActiveRecord::Migration[6.0]
  def change
    add_column :teachers, :kind, :integer
  end
end
